from django.urls import path
from ensembl_rest import views


urlpatterns = [
    path('genes', views.GeneLookup.as_view())
]
