"""
Django development settings for GeneAPI project.
"""

from .base import *

DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '#b9qencr^)(9fmro@okvw)dwun!m^&)-s)f(8lj)%1o*#2vnry'

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        'MANAGED': True
    }
}

REST_FRAMEWORK = {
    # Set 'DEFAULT_PAGINATION_CLASS' value to None to disable pagination
    'DEFAULT_PAGINATION_CLASS': None,
}
