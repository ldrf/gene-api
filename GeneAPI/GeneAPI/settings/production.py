"""
Django production settings for GeneAPI project.
"""

from .base import *

DEBUG = False

# SECURITY WARNING: keep the secret key used in production secret!
# This key should be provided during deployment!
SECRET_KEY = '#b9qencr^)(9fmro@okvw)dwun!m^&)-s)f(8lj)%1o*#2vnry'

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'ensembl_website_97',
        'USER': 'anonymous',
        'PASSWORD': '',
        'HOST': 'ensembldb.ensembl.org',
        'PORT': '3306',
        'MANAGED': False
    }
}


REST_FRAMEWORK = {
    # Set 'DEFAULT_PAGINATION_CLASS' value to None to disable pagination
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 100
}
