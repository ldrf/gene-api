from rest_framework import status
from rest_framework.views import APIView
from rest_framework.settings import api_settings
from rest_framework.response import Response
from ensembl_rest.models import Gene
from ensembl_rest.serializers import GeneSerializer


class GeneLookup(APIView):
    '''
    View for retrieving Gene records.

    Allows method GET.
    Other methods (POST/PUT/PATCH/DELETE) shall receive a 405 Method Not Allowed HTTP error.
    '''
    paginator = api_settings.DEFAULT_PAGINATION_CLASS() \
            if api_settings.DEFAULT_PAGINATION_CLASS is not None else None

    def get(self, request, format=None):
        '''
        Return a JSON response containing a list of Gene records serialized
        with GeneSerializer.

        GET parameters:
            lookup: The search term, must be at least 3 characters long. The
                    search is performed on Gene display_label field.
            species (non mandatory): Filter search results matching exactly
                                     Gene species field.

        Response results are paginated (see https://www.django-rest-framework.org/api-guide/pagination/)
        if DEFAULT_PAGINATION_CLASS is set in application settings.
        Set DEFAULT_PAGINATION_CLASS to None to disable pagination.
        '''
        lookup = request.query_params.get('lookup', None)
        species = request.query_params.get('species', None)
        if self._valid_lookup(lookup):
            genes = self._get_genes(request, lookup, species)
            gene_serializer = GeneSerializer(genes, many=True)
            response = self._get_response(gene_serializer.data)
            return response
        else:
            err = {'detail': '"lookup" parameter must contain at least 3 characters'}
            return Response(err, status=status.HTTP_400_BAD_REQUEST)

    def _valid_lookup(self, lookup):
        return lookup is not None and len(lookup) >= 3

    def _get_genes(self, request, lookup, species):
        genes = Gene.objects.filter(display_label__icontains=lookup)
        if species is not None:
            genes = genes.filter(species__iexact=species)
        if self.paginator is not None:
            genes = self.paginator.paginate_queryset(genes, request)
        return genes

    def _get_response(self, data):
        if self.paginator is None:
            response = Response(data)
        else:
            response = self.paginator.get_paginated_response(data)
        return response
