from ensembl_rest.models import Gene
from rest_framework import serializers


class GeneSerializer(serializers.ModelSerializer):
    '''
    Serializer for Gene model.

    stable_id => ensembl_stable_id
    species => species
    display_label => gene_names
    location => location

    db is excluded
    '''
    gene_names = serializers.CharField(source='display_label')
    ensembl_stable_id = serializers.CharField(source='stable_id')

    class Meta:
        model = Gene
        exclude = ('db', 'stable_id', 'display_label')

