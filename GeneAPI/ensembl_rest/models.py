from django.db import models
from django.conf import settings


class Gene(models.Model):
    '''
    Model for Ensembl 97 Database gene_autocomplete table
    '''
    stable_id = models.CharField(max_length=128, primary_key=True)
    species = models.CharField(max_length=255, blank=True, null=True)
    display_label = models.CharField(max_length=128, blank=True, null=True)
    location = models.CharField(max_length=60, blank=True, null=True)
    db = models.CharField(max_length=32)

    class Meta:
        managed = settings.DATABASES['default']['MANAGED']
        db_table = 'gene_autocomplete'

    def __str__(self):
        return f'SID: {self.stable_id}'
