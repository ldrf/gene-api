from django.apps import AppConfig


class EnsemblRestConfig(AppConfig):
    name = 'ensembl_rest'
