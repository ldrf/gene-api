from django.test import TestCase, override_settings
from rest_framework.test import APIRequestFactory
from ensembl_rest.models import Gene
from ensembl_rest.serializers import GeneSerializer
from ensembl_rest.views import GeneLookup


class GeneSerializerTestCase(TestCase):
    def setUp(self):
        Gene.objects.create(stable_id='TESTID00000000001',
                            species='test_species',
                            display_label='testgene1',
                            location='TEST:LOC.1',
                            db='testdb')

    def test_gene_serialization(self):
        gene = Gene.objects.get(stable_id='TESTID00000000001')
        expected = {
            'gene_names': 'testgene1',
            'ensembl_stable_id': 'TESTID00000000001',
            'species': 'test_species',
            'location': 'TEST:LOC.1'
        }
        serialized = GeneSerializer(gene)
        self.assertEqual(serialized.data, expected)

    def tearDown(self):
        Gene.objects.all().delete()


class GeneLookupTestCase(TestCase):
    def setUp(self):
        Gene.objects.create(stable_id='TESTID00000000001',
                            species='test_species1',
                            display_label='testgene1',
                            location='TEST:LOC.1',
                            db='testdb')
        Gene.objects.create(stable_id='TESTID00000000002',
                            species='test_species2',
                            display_label='TESTGENE2',
                            location='TEST:LOC.2',
                            db='testdb')
        self.request_factory = APIRequestFactory()
        self.view = GeneLookup.as_view()

    def test_success_get(self):
        request = self.request_factory.get('genes?lookup=test')
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 2)
        response.data.sort(key=lambda x: x['ensembl_stable_id'])
        self.assertEquals(response.data[0]['ensembl_stable_id'], 'TESTID00000000001')
        self.assertEquals(response.data[1]['ensembl_stable_id'], 'TESTID00000000002')

    def test_success_get_with_species(self):
        request = self.request_factory.get('genes?lookup=test&species=test_species2')
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEquals(response.data[0]['ensembl_stable_id'], 'TESTID00000000002')

    def test_fail_get_with_no_lookup(self):
        request = self.request_factory.get('genes')
        response = self.view(request)
        self.assertEquals(response.status_code, 400)

    def test_fail_get_with_short_lookup(self):
        request = self.request_factory.get('genes?lookup=te')
        response = self.view(request)
        self.assertEquals(response.status_code, 400)

    def test_fail_post(self):
        request = self.request_factory.post('genes', {'ensembl_stable_id': '000'})
        response = self.view(request)
        self.assertEquals(response.status_code, 405)

    def test_fail_put(self):
        request = self.request_factory.put('genes', {'ensembl_stable_id': '000'})
        response = self.view(request)
        self.assertEquals(response.status_code, 405)

    def test_fail_patch(self):
        request = self.request_factory.patch('genes', {'ensembl_stable_id': '000'})
        response = self.view(request)
        self.assertEquals(response.status_code, 405)

    def test_fail_delete(self):
        request = self.request_factory.delete('genes', {'ensembl_stable_id': '000'})
        response = self.view(request)
        self.assertEquals(response.status_code, 405)

    def tearDown(self):
        Gene.objects.all().delete()

