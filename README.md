# Gene API

REST API interface over ENSEMBL Website 97 Database written in Python (Django &
Django REST Framework)


## Description

Implements a HTTP REST endpoint for searching genes by their names. Only GET
method is allowed. The entry point `genes` accepts a string `lookup` as
parameter and return the list of matching genes in Ensembl database.

Example query: `http://my-domain.org/genes?lookup=brc`

An optional parameter `species` allow to filter the results by target species.

Response results are paginated (see [Django REST framework Pagination Guide](https://www.django-rest-framework.org/api-guide/pagination/))
as some queries might return thousands of records.

Pagination can be disabled via Django settings.


## Requirements

- Python 3.6+
- Django 2.2+
- Django REST Framework 3.10+
- MySQL Client


## Installing requirements

Requirements can be installed using `pip`.

To install only production requirements (no development/test tools) run:
```
pip install -r requirements/prod.txt
```

To install all development and testing requirements:
```
pip install -r requirements/dev.txt
```

I recommend using virtualenv (or similar)


## Usage

GeneAPI can be easily connected to either the remote database
`ensembl_website_97` or the local development sqlite3 instance, the latter is
useful for development and unit testing.

Running:
```
cd GeneAPI
python manage.py runserver
```

will run Django development server
connected to the local sqlite3 instance. The default settings file that is
loaded is `settings/development.py`.

To connect to the remote database or in general run with production settings
simply explicitly pass `--settings=GeneAPI.settings.production` when invoking
`manage.py` commands.

Running:
```
cd GeneAPI
python manage.py runserver --settings=GeneAPI.settings.production
```

will run the development server connected to the remote `ensembl_website_97`
and with production settings. This is useful when running integration tests.


## Deployment

A good way to deploy GeneAPI is using linux containers. As GeneAPI is
stateless, several containers could be deployed behind a load balancer in
order to scale horizontally (eventually spawning more containers) to meet
increased demand without worrying to maintain sessions.

A `Dockerfile` is provided in order to build a docker image for production.

If Docker is available, an image can be built and run with:
```
docker build -t gene-api .
docker run -p 8080:8000 -e GUNICORN_CMD_ARGS="--bind=0.0.0.0 --workers=4" gene-api
```

Note that that service will listen to port 8080 instead of the development port
8000.

Depending on how and how often the data change in the database, using a simple
LRU cache could sensibly increase the throughput.

As it is GeneAPI doesn't come with any logging or monitor facilities. If
deployed as containers then an external logging tool would be needed to catch
the container's output and send it to a log aggregator.


## Testing

GeneAPI comes with two sets of tests.


#### Unit Tests

Unit tests are run using a local mock of the database in order to quickly tests
the business logic of the application and without the need of connecting to
other services. Unit tests aren't performed through HTTP but directly via the
Django view.

Unit tests can be run with:
```
cd GeneAPI
python manage.py test
```


#### Integration Tests

Integration tests are run using production settings and send actual http
requests to the server (the server must be runnung and accessible).

In the folder `integration_tests` a small test suite built using *pytest* and
*requests* is provided.

To run the itegration tests, first start the development server with:
```
cd GeneAPI
python manage.py runserver --settings=GeneAPI.settings.production
```
then in a separate terminal run:
```
cd integration_tests
pytest -q test_lookup.py
```

By default the integration tests will hit localhost on port 8000. Alternative
host name and port can be passed to the test with the flags `--host` and
`--port`.

For example, to test a container running on localhost listening to 8080
run:
```
cd integration_tests
pytest -q test_lookup.py --port 8080
```

Test should be automated as part of the CI, unit tests should be triggered
every time a set of commits is pushed to the remote repository. Integration
tests should be triggered every time a build is made, using a staging area and
a replica of the production database if possble, or the closest thing
available.


## Message Queue

In order to access GeneAPI service asynchronously using a queue such as
RabbitMQ, I would suggest to implement an amqp consumer interface alongside the
rest interface as a separate Django application (called ensembl\_amqp) within
GeneAPI project (using something like *Pika*). The consumer would run as a
separate process (with the help of *Celery* perhaps) but share the same model
and serializers as ensembl\_rest.

Incoming messages could be formatted as:

```json
{
    "lookup": "brc",
    "species": ""
}
```

Outgoing messages coul be formatted as:
```json
{
    "lookup": "brc",
    "species": "",
    "results": [
        {
            "gene_names": "BRCA1",
            "species": "homo_sapiens",
            "location": "LOC.1:0",
            "ensembl_stable_id": "ID00001234"
        },
        ...
    ]
}
```

Unit tests for the amqp methods will need to be added as well as integration
tests. For the integration tests, a staging area with testing queues (staging
virtual host in RabbitMQ broker) and GeneAPI deployed and connected to the
staging database would be good.
