FROM debian:buster-slim
RUN apt-get update && \
    apt-get install -y python3 gunicorn3 python3-pip default-libmysqlclient-dev && \
    rm -rf /var/lib/apt/lists/* && \
    mkdir -p /opt/app

COPY GeneAPI /opt/app/
COPY requirements/prod.txt /opt/app/requirements.txt

WORKDIR /opt/app
RUN pip3 install -r requirements.txt
EXPOSE 8000
ENTRYPOINT ["gunicorn3", "GeneAPI.wsgi:application"]
