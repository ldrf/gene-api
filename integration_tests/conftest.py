import pytest
import urllib


def pytest_addoption(parser):
    parser.addoption(
        '--host', action='store', default='127.0.0.1', help='GeneAPI host name'
    )
    parser.addoption(
        '--port', action='store', default='8000', help='GeneAPI host port'
    )

@pytest.fixture
def server(request):
    host = request.config.getoption('--host')
    port = request.config.getoption('--port')
    return f'{host}:{port}'
