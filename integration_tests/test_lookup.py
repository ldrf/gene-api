import json
import re
import requests


def test_successful_get(server):
    response = requests.get(f'http://{server}/genes?lookup=brc')
    assert response.status_code == 200
    content = json.loads(response.content)
    results = content['results']
    for name in map(lambda x: x['gene_names'], results):
        assert re.search('brc', name, re.IGNORECASE) is not None


def test_fail_get(server):
    response = requests.get(f'http://{server}/genes?lookup=br')
    assert response.status_code == 400


def test_fail_post(server):
    response = requests.post(f'http://{server}/genes')
    assert response.status_code == 405


def test_fail_put(server):
    response = requests.put(f'http://{server}/genes')
    assert response.status_code == 405


def test_fail_patch(server):
    response = requests.patch(f'http://{server}/genes')
    assert response.status_code == 405


def test_fail_delete(server):
    response = requests.delete(f'http://{server}/genes')
    assert response.status_code == 405

